#ifndef _scoped_ptr_hpp_include_
#define _scoped_ptr_hpp_include_

#include "utils.hpp"

/// Default deleter, just calls delete. Use it when allocating memory through
/// regular `new`
template<typename T>
struct default_deleter {
	void operator()(T* p){ delete p; }
};

/// This deleter calls the pointer's type destructor, but does not call delete.
/// This is recommended if you use a memory arena or some other custom allocator
/// as a memory management strategy but needs non-trivial deinitialization.
template<typename T>
struct deinit_deleter {
	void operator()(T* p){ p->~T(); }
};

/// A pointer that calls a Deleter functor when it goes out of scope.
template<typename T, typename Deleter = default_deleter<T>>
struct scoped_ptr : Deleter {
	T* ptr;

	T* raw_data(void) const {
		return ptr;
	}

	Deleter get_deleter(void) const {
		return Deleter::op;
	}

	scoped_ptr(void) : ptr{nullptr}, Deleter{} {}

	scoped_ptr(T* p) : ptr{p} {}
	scoped_ptr(T* p, Deleter const& d) : ptr{p}, Deleter(d){}

	T& operator*(void) const {
		return *raw_data();
	}

	operator bool(void){
		return static_cast<bool>(ptr);
	}

	bool operator==(scoped_ptr const&){
		// Scoped pointers by definition are not meant to point to the same thing
		return false;
	}

	T* operator->(void) const {
		return raw_data();
	}

	T& operator[](int i){
		// TODO: change to size_t
		return ptr[i];
	}

	/// Copy operations (prohibit) ///
	scoped_ptr(scoped_ptr const&) = delete;

	void operator=(scoped_ptr const&) = delete;

	/// Move operations ///
	scoped_ptr(scoped_ptr&& p) : ptr{p.release()} {}

	void operator=(scoped_ptr&& p){
		if(this != &p){
			reset(p.release());
		}
	}

	/// Releases ownership of pointer
	T* release(){
		return exchange(ptr, nullptr);
	}

	/// Calls the deleter on current data (if any) and then take ownership
	/// of new pointer
	void reset(T* p = nullptr){
		T* old = ptr;
		ptr = p;
		if(old != nullptr){
			Deleter::operator()(ptr);
		}
	}

	~scoped_ptr(){
		if(ptr != nullptr){
			Deleter::operator()(ptr);
		}
	}
};

template<typename T, typename Deleter = default_deleter<T>, typename ...CtorArgs>
scoped_ptr<T, Deleter> make_scoped(CtorArgs&& ...args){
	auto p = new T(forward<CtorArgs>(args)...);
	return scoped_ptr<T>(p);
}

#endif /* Include guard */

