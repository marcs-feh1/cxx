#ifndef _array_hpp_include_
#define _array_hpp_include_

#include "types.hpp"
#include "utils.hpp"

namespace x {
template<typename T, usize N>
struct array {
	T data[N];

	constexpr
	usize size() const { return N; }

	constexpr
	T& operator[](usize i){
		return data[i];
	}

	constexpr
	T const& operator[](usize i) const{
		return data[i];
	}

	template<typename Fn>
	constexpr
	auto map(Fn&& f) const {
		array<decltype(f(data[0])), N> res;
		for(usize i = 0; i < N; i += 1){
			res[i] = f(data[i]);
		}
		return res;
	}

	template<typename Fn, typename U>
	constexpr
	T fold(Fn&& func, U&& inital_val) const {
		static_assert(N > 1, "To use fold() the array must be at least of size 2");

		T x = func(x::forward<U>(inital_val), data[0]);

		for(usize i = 1; i < N; i += 1){
			x = func(x, data[i]);
		}

		return x;
	}

	template<typename Fn>
	constexpr
	usize count(Fn&& predicate) const {
		usize n = 0;
		for(usize i = 0; i < N; i += 1){
			if(predicate(data[i])){ n += 1; }
		}
		return n;
	}

	constexpr
	bool equals(array<T, N> const& v){
		for(usize i = 0; i < N; i += 1){
			if(v[i] != data[i]){ return false; }
		}

		return true;
	}

};

/// Vector Operations ///
// Arithmetic //
template<typename T, usize N>
array<T, N> operator+(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] + b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator-(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] - b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator-(array<T, N> const& a){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = -a[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator*(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] * b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator/(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] / b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator%(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] % b[i];
	}
	return res;
}

// Bitwise //
template<typename T, usize N>
array<T, N> operator&(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] & b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator|(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] | b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator^(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] ^ b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator~(array<T, N> const& a){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = ~a[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator<<(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] << b[i];
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator>>(array<T, N> const& a, array<T, N> const& b){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] >> b[i];
	}
	return res;
}

// Boolean //
template<typename T, usize N>
array<T, N> operator!(array<T, N> const& a){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = !a[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator<(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] < b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator<=(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] <= b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator>(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] > b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator>=(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] >= b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator==(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] == b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator!=(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] != b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator&&(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] && b[i];
	}
	return res;
}

template<typename T, usize N>
array<bool, N> operator||(array<T, N> const& a, array<T, N> const& b){
	array<bool, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] && b[i];
	}
	return res;
}

/// Vector - Scalar operations ///
// Arithmetic //
template<typename T, usize N>
array<T, N> operator+(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] + s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator-(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] - s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator*(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] * s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator/(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] / s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator%(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] % s;
	}
	return res;
}

// Bitwise //
template<typename T, usize N>
array<T, N> operator&(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] & s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator|(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] | s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator^(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] ^ s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator<<(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] << s;
	}
	return res;
}

template<typename T, usize N>
array<T, N> operator>>(array<T, N> const& a, T const& s){
	array<T, N> res;
	for(usize i = 0; i < N; i += 1){
		res[i] = a[i] >> s;
	}
	return res;
}
}
#endif /* Include guard */
