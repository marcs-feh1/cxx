CXX    := clang++ -std=c++17
LD     := clang++ -fuse-ld=mold
CFLAGS := -O1 -pipe -march=native
LDFLAGS :=

override CFLAGS := $(CFLAGS) -I. -Wall -Wextra 
override LDFLAGS := $(LDFLAGS) -L.

.PHONY: run clean

run: test.bin
	@./test.bin

test.o: test.cpp tests/* *.hpp
	$(CXX) $(CFLAGS) -c test.cpp -o test.o

test.bin: test.o
	$(LD) $(LDFLAGS) test.o -o test.bin


clean:
	rm -f *.o *.so *.dll *.exe *.bin *.out
