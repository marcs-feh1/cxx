#ifndef _slice_hpp_include_
#define _slice_hpp_include_

#include "types.hpp"
#include "panic.hpp"
#include "option.hpp"

namespace x {
template<typename T>
struct slice {
	T* data;
	usize len;

	constexpr
	usize size() const { return len; }

	constexpr
	T* raw_data() const { return data; }

	/// A null slice is one which points to nullptr OR has a lenght == 0
	constexpr
	bool null() const { return (data == nullptr) || (len == 0); }

	constexpr
	slice() : data{0}, len{0} {}

	constexpr
	slice(T* ptr, usize n) : data{ptr}, len{n} {}

	constexpr
	slice(slice const& s) : data{s.data}, len{s.len} {}

	constexpr
	void operator=(slice const& s){
		data = s.data;
		len = s.len;
	}

	constexpr
	slice(slice&& s){
		data = exchange(s.data, nullptr);
		len = exchange(s.len, 0);
	}

	constexpr
	void operator=(slice&& s){
		data = exchange(s.data, nullptr);
		len = exchange(s.len, 0);
	}

	// Provides a safe wrapper to access things with bounds checking, the
	// boolean value is used to verify if it is in-bounds.
	constexpr
	pair<T&, bool> at(usize idx){
		if(idx >= len){
			return {data[0], false};
		}
		return {data[idx], true};
	}

	/// Provides a safe wrapper to access things with bounds checking, the
	/// boolean value is used to verify if it is in-bounds.
	constexpr
	pair<T const&, bool> at(usize idx) const {
		if(idx >= len){
			return {data[0], false};
		}
		return {data[idx], true};
	}

	/// Bounds checked access if NO_BOUNDS_CHECK is not defined
	constexpr
	T& operator[](usize idx){
		bounds_check(idx < len, "Slice index out of bounds");
		return data[idx];
	}

	/// Bounds checked access if NO_BOUNDS_CHECK is not defined
	constexpr
	T const& operator[](usize idx) const {
		bounds_check(idx < len, "Slice index out of bounds");
		return data[idx];
	}

	/// Slices are equal if they have the same size and equal elements
	constexpr
	bool equals(slice<T> const& e) const {
		if(e.len != len){ return false; }
		for(usize i = 0; i < len; i += 1){
			if(e.data[i] != data[i]){ return false; }
		}
		return true;
	}

	constexpr
	bool operator==(slice const& e) const {
		return equals(e);
	}

	constexpr
	bool operator!=(slice const& e) const {
		return !equals(e);
	}

	constexpr
	operator bool(){
		return !null();
	}

};
};

#endif /* Include guard */
