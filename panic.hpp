#ifndef _panic_hpp_include_
#define _panic_hpp_include_

#include <cstdio>
#include <cstdlib>

namespace x {

/// Causes the program to die. this is a thin wrapper around your desired "crash"
/// function
static inline
void die(void){
	do {
		std::abort();
	} while(true);
}

/// Crashes the program and prints a message stderr
static inline
void panic(char const * const msg){
	fprintf(stderr, "Panic: %s\n", msg);
	die();
}

/// Assert that is always enabled, regardless of build mode
static inline
void panic_assert(bool cond, char const* const msg){
	if(!cond){
		panic(msg);
	}
}

/// Same as panic_assert, gets disabled NO_BOUNDS_CHECK is defined
static inline
void bounds_check(bool cond, char const* const msg) {
#ifdef NO_BOUNDS_CHECK
	(void)cond;
	(void)msg;
#else
	panic_assert(cond, msg);
#endif
}

/// Panic that is only enabled for non-release builds
static inline
void debug_panic(char const * const msg){
#if defined(RELEASE_MODE) || defined(NDBUG)
	(void)msg;
#else
	panic(msg);
#endif
}

/// Assert that is only enabled for non-release builds
static inline
void debug_assert(bool cond, char const * const msg){
#if defined(RELEASE_MODE) || defined(NDEBUG)
	(void)msg;
	(void)cond;
#else
	panic_assert(cond, msg);
#endif
}

}

#endif /* Include guard */
