#ifndef _types_hpp_include_
#define _types_hpp_include_

#include <cstddef>
#include <cstdint>

using i8  = std::int8_t;
using i16 = std::int16_t;
using i32 = std::int32_t;
using i64 = std::int64_t;

using u8  = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;

using least_i8  = std::int_least8_t;
using least_i16 = std::int_least16_t;
using least_i32 = std::int_least32_t;
using least_i64 = std::int_least64_t;

using least_u8  = std::uint_least8_t;
using least_u16 = std::uint_least16_t;
using least_u32 = std::uint_least32_t;
using least_u64 = std::uint_least64_t;

using uintptr = std::uintptr_t;
using usize = std::size_t;
using isize = std::ptrdiff_t;
static_assert(sizeof(usize) == sizeof(isize), "Mismatch sizes for usize and isize.");

using f16 = _Float16;
using f32 = float;
using f64 = double;

static_assert(
	(sizeof(f16) == 2) &&
	(sizeof(f32) == 4) &&
	(sizeof(f64) == 8),
	"Floating point types have improper sizes.");

using uint = unsigned int;
using rune = u32;
using byte = u8;

#endif /* Include guard */
