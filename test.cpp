#include <iostream>
#include <utility>
#include <functional>

#include "types.hpp"
#include "panic.hpp"
#include "array.hpp"
#include "slice.hpp"

#include "mem/mem.hpp"

#include "tests/monadic_types.cpp"
#include "tests/array.cpp"
#include "tests/slice.cpp"

int main(){
	int s =
		+ test_option()
		+ test_result()
		+ test_array()
		+ test_slice()
	;

	return s;
}
