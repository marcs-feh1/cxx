#ifndef _option_hpp_include_
#define _option_hpp_include_

#include "utils.hpp"
#include "panic.hpp"
namespace x {

template<typename T>
struct option {
	enum struct error {
		Nil_Value,
	};

	union {
		T val;
	};
	bool has_value;

	constexpr
	bool ok() const { return has_value; }

	template<typename Func>
	constexpr
	auto apply(Func&& f){
		using U = decltype(f(val));
		if(has_value){
			return option<U>(f(val));
		} else {
			return option<U>();
		}
	}

	/// Returns value if it has one, otherwise raise an error.
	constexpr
	T get() const& {
		if(!has_value){
			panic("Attempt to get() nil value");
		}

		return val;
	}

	/// Moves value out if it has one, otherwise raise an error.
	constexpr
	T get() && {
		if(!has_value){
			panic("Attempt to get() nil value");
		}
		has_value = false;
		return x::move(val);
	}

	/// Returns value if it has one, otherwise return alt.
	constexpr
	T get_or(T const& alt) const& {
		if(has_value){
			return val;
		} else {
			return alt;
		}
	}

	/// Moves out value if it has one, otherwise return alt.
	constexpr
	T&& get_or(T&& alt) && {
		if(has_value){
			return x::move(val);
		} else {
			return x::move(alt);
		}
	}

	constexpr
	void destroy(){
		if(has_value){
			val.~T();
		}
		has_value = false;
	}

	option() : has_value{false} {}

	/// Construct from optional ///

	option(option<T> const& opt){
		has_value = opt.has_value;
		if(has_value){
			new (&val) T(opt.val);
		}
	}

	option(option<T>&& opt){
		has_value = x::exchange(opt.has_value, false);
		if(has_value){
			new (&val) T(x::move(opt.val));
		}
	}

	/// Construct from value ///

	option(T&& v) : has_value {true} {
		new (&val) T(x::move(v));
	}

	option(T const& v) : has_value {true} {
		new (&val) T(v);
	}


	/// Assign to value ///

	constexpr
	void operator=(T const& v){
		if(!has_value){
			new (&val) T(v);
		} else {
			val = v;
		}
		has_value = true;
	}

	constexpr
	void operator=(T&& v){
		if(!has_value){
			new (&val) T(x::move(v));
		} else {
			val = x::move(v);
		}
		has_value = true;
	}

	/// Assign to optional ///

	constexpr
	void operator=(option<T> const& opt){
		if(opt.has_value){
			*this = opt.val;
		} else {
			this->destroy();
		}
	}

	constexpr
	void operator=(option<T>&& opt){
		if(opt.has_value){
			*this = x::move(opt.val);
		} else {
			destroy();
		}
		has_value = x::exchange(opt.has_value, false);
	}

	~option(){
		if(has_value){
			val.~T();
		}
	}
};
}

#endif /* Include guard */
