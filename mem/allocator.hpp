#ifndef _allocator_hpp_include_
#define _allocator_hpp_include_

#include "mem/mem.hpp"

/// Allocator interface
#include "internal/allocator.hpp"

/// WARN: fwd...

namespace x {
template<typename T, typename ...CtorArgs>
T* make(Allocator al, CtorArgs&&... args){
	auto ptr = static_cast<T*>(al.alloc(sizeof(T), alignof(T)));
	if(ptr != nullptr){
		construct_at(ptr, forward<CtorArgs>(args)...);
	}
	return ptr;
}

template<typename T, typename ...CtorArgs>
slice<T> make_slice(Allocator al, usize n, CtorArgs&&... args){
	auto ptr = static_cast<T*>(al.alloc(sizeof(T) * n, alignof(T)));
	if(ptr != nullptr){
		for(usize i = 0; i < n; i += 1){
			construct_at(&ptr[i], args...);
		}
	}
	return slice(ptr, n);
}

template<typename T>
void destroy(Allocator al, T* addr){
	al.dealloc(addr);
	if(addr != nullptr){
		addr->~T();
	}
}

template<typename T>
void destroy(Allocator al, slice<T> items){
	al.dealloc(items.raw_data());
	if(items){
		auto n = items.size();
		for(usize i = 0; i < n; i += 1){
			items[i].~T();
		}
	}
}

}


#endif /* Include guard */
