#ifndef _exchange_hpp_include_
#define _exchange_hpp_include_

namespace x {

namespace typing {
template<typename T>
struct remove_reference_type {typedef T Type; };
template<typename T>
struct remove_reference_type<T&> {typedef T Type; };
template<typename T>
struct remove_reference_type<T&&> {typedef T Type; };

template<typename T, T v>
struct integral_const {
	static constexpr T value = v;
	typedef T value_type;
	constexpr operator value_type() { return value; }
};

using true_type  = integral_const<bool, true>;
using false_type = integral_const<bool, false>;

template<typename A, typename B>
struct same_as_type : false_type {};

template<typename T>
struct same_as_type<T, T> : true_type {};

template<typename A, typename B>
constexpr auto same_as = same_as_type<A, B>::value;

template<typename T>
struct is_lval_ref_type : false_type {};
template<typename T>
struct is_lval_ref_type<T&> : true_type {};
template<typename T>
struct is_lval_ref_type<T&&> : false_type {};

template<typename T>
struct is_rval_ref_type : false_type {};
template<typename T>
struct is_rval_ref_type<T&> : false_type {};
template<typename T>
struct is_rval_ref_type<T&&> : true_type {};
}

template<typename T>
using remove_reference = typename typing::remove_reference_type<T>::Type;

template<typename T>
constexpr bool is_lvalue_ref = typing::is_lval_ref_type<T>::value;

template<typename T>
constexpr bool is_rvalue_ref = typing::is_rval_ref_type<T>::value;

/// Cast x to rvalue reference
template<typename T>
constexpr remove_reference<T>&& move(T&& x){
	using Rv = remove_reference<T>&&;
	return static_cast<Rv>(x);

}

/// Contitionally moves x, if and only if, x is an rvalue reference. Requires
/// passing template type explicitly. This is used to implement "perfect forwarding"
template<typename T>
constexpr T&& forward(remove_reference<T>& x){
	return static_cast<T&&>(x);
}

/// Contitionally moves x, if and only if, x is an rvalue reference. Requires
/// passing template type explicitly. This is used to implement "perfect forwarding"
template<typename T>
constexpr T&& forward(remove_reference<T>&& x){
	static_assert(!is_lvalue_ref<T>, "Cannot use forward() to convert an rvalue to an lvalue");
	return static_cast<T&&>(x);
}

/// Puts val into x and return x's old value
template<typename T, typename U = T>
T exchange(T& x, U&& val){
	T t = move(x);
	x   = forward<U>(val);
	return t;
}

/// Swap values of a and b
template<typename T>
constexpr
void swap(T& a, T& b){
	T t = move(b);
	b   = move(a);
	a   = move(t);
}

/// Does what you think it does.
template<typename T>
T min(const T& a, const T& b){
	if(a < b){ return a; }
	return b;
}

/// Does what you think it does (varargs edition).
template<typename T, typename ...Rest>
T min(const T& a, const T& b, const Rest& ...rest){
	if(a < b){
		return min(a, rest...);
	}
	else {
		return min(b, rest...);
	}
}

/// Does what you think it does.
template<typename T>
T max(const T& a, const T& b){
	if(a > b){ return a; }
	return b;
}

/// Does what you think it does (varargs edition).
template<typename T, typename ...Rest>
T max(const T& a, const T& b, const Rest& ...rest){
	if(a > b){
		return max(a, rest...);
	}
	else {
		return max(b, rest...);
	}
}

/// Ensure that x's value is in between a and b (inclusive).
template<typename T>
T clamp(const T& a, const T& x, const T& b){
	if(x < a){ return a; }
	if(x > b){ return b; }
	return x;
}

template<typename A, typename B>
struct pair {
	A a;
	B b;
};

}

#endif /* Include guard */
