#ifndef _result_hpp_include_
#define _result_hpp_include_

// TODO: reference_wrapper???

#include "utils.hpp"
#include "panic.hpp"

namespace x {

template<typename Val, typename Err>
struct result {
	union {
		Val val;
		Err err;
	};
	enum struct error {
		No_Value,
		No_Error,
	};
	bool is_val;

	bool ok() const {
		return is_val;
	}

	/// Construction ///
	constexpr
	result() : is_val{false}, err{0} {}

	constexpr
	result(Val const& v) : is_val{true} {
		new (&val) Val(v);
	}

	constexpr
	result(Val && v) : is_val{true} {
		new (&val) Val(x::move(v));
	}

	constexpr
	result(Err const& e) : is_val{false} {
		new (&err) Err(e);
	}

	constexpr
	result(Err && e) : is_val{false} {
		new (&err) Err(x::move(e));
	}

	constexpr
	result(result<Val, Err> const& r) : is_val{r.is_val} {
		if(is_val){
			new (&val) Val(r.val);
		} else {
			new (&err) Err(r.err);
		}
	}

	constexpr
	result(result<Val, Err>&& r) : is_val{r.is_val} {
		if(is_val){
			new (&val) Val(x::move(r.val));
		} else {
			new (&err) Err(x::move(r.err));
		}
	}

	/// Assign to value ///
	constexpr
	void operator=(Val const& v){
		if(is_val){
			val = v;
		} else {
			err.~Err();
			new (&val) Val(v);
		}
		is_val = true;
	}
	constexpr
	void operator=(Val&& v){
		if(is_val){
			val = x::move(v);
		} else {
			err.~Err();
			new (&val) Val(x::move(v));
		}
		is_val = true;
	}

	/// Assign to error ///
	constexpr
	void operator=(Err const& e){
		if(!is_val){
			err = e;
		} else {
			val.~Val();
			new (&err) Err(e);
		}
		is_val = false;
	}

	/// Assign to result ///
	constexpr
	void operator=(result<Val, Err> const& r){
		if(r.is_val){
			*this = r.val;
		} else {
			*this = r.err;
		}
	}

	template<typename Func>
	constexpr
	auto apply(Func&& f){
		using U = decltype(f(val));
		if(is_val){
			// C++ moment.
			return result<Val, Err>(static_cast<Val>(x::forward<U>(f(val))));
		} else {
			return *this;
		}
	}

	/// Moves value out if it has one, otherwise raise an error.
	constexpr
	Val get() const& {
		if(!is_val){
			// TODO: panic
			panic("Attempt to get unavailable value");
		}
		return val;
	}

	/// Moves value out if it has one, otherwise raise an error.
	constexpr
	Val get() && {
		if(!is_val){
			// TODO: panic
			panic("Attempt to get unavailable value");
		}
		return x::move(val);
	}

	constexpr
	Err error(){
		if(is_val){
			// TODO: panic
			panic("Attempt to get unavailable error");
		}
		return err;
	}

	constexpr
	Val get_or(Val && alt){
		if(is_val){
			return x::move(val);
		} else {
			return x::move(alt);
		}
	}

	constexpr
	Val get_or(Val const& alt){
		if(is_val){
			return val;
		} else {
			return alt;
		}
	}


	~result(){
		if(is_val){
			val.~Val();
		} else {
			err.~Err();
		}
	}

};

}


#endif /* Include guard */
