#include "test_urself.hpp"
#include "slice.hpp"

static int test_slice(){
	using namespace x;
	Test_Begin("Slice");

	auto nums_ = new int[400];
	auto nums = slice<int>(nums_, 400);
	Tp(nums.size() == 400);
	Tp(nums.raw_data() != nullptr);
	{
		auto [v, ok] = nums.at(1000);
		Tp(!ok);
	}
	{
		auto [v, ok] = nums.at(5);
		Tp(ok);
		v = 69;
		Tp(nums[5] == 69);
	}
	{
		auto nums0 = nums;
		Tp(nums0.equals(nums));
		auto nums1 = x::move(nums);
		Tp(nums1 != nums);
		Tp(nums1 == nums0);
		Tp(nums.raw_data() == nullptr);
		Tp(nums.size() == 0);
	}

	delete[] nums_;
	Test_End();
}
