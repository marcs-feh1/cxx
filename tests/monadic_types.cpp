#include "test_urself.hpp"

#include <memory>
#include <array>

#include "option.hpp"
#include "result.hpp"
#include "types.hpp"
#include "utils.hpp"

struct MoveOnly{
	char payload[256];

	MoveOnly() : payload{0} {}

	MoveOnly(MoveOnly const&) = delete;
	MoveOnly& operator=(MoveOnly const&) = delete;

	MoveOnly(MoveOnly&&) : payload{0} {};
	MoveOnly& operator=(MoveOnly &&){ return *this; }
};

static int test_option(){
	using namespace x;
	Test_Begin("Option");
	auto safe_div = [](int a, int b) -> option<int>{
		if(b == 0){
			return option<int>();
		}
		return option<int>(a/b);
	};


	{
		int a = 4;
		int b = 0;
		auto q = safe_div(a,b);
		Tp(q.get_or(69) == 69);

		Tp(q.apply([](int a){ return a+1; })
	 .get_or(69) == 69);
	}

	{
		int a = 5;
		int b = 2;
		auto q = safe_div(a, b);
		Tp(q.get_or(69) == 2);
		Tp(q.get() == 2);
		Tp(q.apply([](int a){ return a+1; })
	 		.get_or(69) == 3);
	}

	{
		MoveOnly m;
		auto x = option<MoveOnly>(move(m));
		Tp(m.payload[0] == 0);
		auto y = move(x);
		Tp(move(y).get().payload[0] == 0);
		Tp(!y.ok());
		y = MoveOnly();
		Tp(y.ok());
		auto z = move(y);
		Tp(z.ok() && !y.ok());
	}
	{
		auto make_arr = [](int n) -> auto {
			auto e = std::make_unique<std::array<int, 30>>();
			if(n % 2 == 0){
				return option<decltype(e)>(x::move(e));
			} else {
				return option<decltype(e)>();
			}
		};

		auto p = make_arr(1);
		Tp(!p.ok());
		auto e = x::move(p).get_or(std::make_unique<std::array<int, 30>>());
		Tp(e != nullptr);
	}

	Test_End();
}

static int test_result(){
	using namespace x;
	Test_Begin("Result");
	enum struct Error {
		YourMum,
		BadThing,
	};

	auto safe_div = [](f64 a, f64 b){
		if(b == 0){
			return result<f64, Error>(Error::YourMum);
		}
		return result<f64, Error>(a / b);
	};

	{
		f64 a = 32;
		f64 b = 0.5;

		{
			auto x = safe_div(a, b).get_or(4);
			Tp(x != 4);
			x = safe_div(1, 2).get();
			Tp(x == 0.5);
		}

		{
			auto x = safe_div(a, 0.25).apply( [](int e){
				return e - 100;
			}).get_or(69);
			Tp(x == 28);
		}

		{
			auto x = safe_div(a, 0).apply( [](int e){
				return e - 200;
			}).get_or(69);
			Tp(x == 69);
			auto r = safe_div(b, 0);
		}
	}

	{
		auto ra = result<i32, Error>(10);
		auto rb = result<i32, Error>(Error::BadThing);

		Tp(ra.ok() && !rb.ok());

		ra = rb;

		Tp(!ra.ok() && !rb.ok());

		rb = 100;
		Tp(rb.get() == 100);
	}

	{
		auto m = MoveOnly();
		auto rm = result<MoveOnly, u8>(move(m));
		Tp(rm.ok());
		rm = u8(0);
		Tp(!rm.ok());
	}

	{
		auto make_arr = [](int n) -> auto {
			auto e = std::make_unique<std::array<int, 30>>();
			if(n % 2 == 0){
				return result<decltype(e), Error>(x::move(e));
			} else {
				return result<decltype(e), Error>(Error::BadThing);
			}
		};

		auto p = make_arr(1);
		Tp(!p.ok());
		auto e = x::move(p).get_or(std::make_unique<std::array<int, 30>>());
		Tp(e != nullptr);
	}

	Test_End();
}

