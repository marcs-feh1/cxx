#include "test_urself.hpp"

#include "array.hpp"
#include "types.hpp"
#include "utils.hpp"

static int test_array(){
	Test_Begin("Array");
	using namespace x;
	{
		auto v0 = array<f64, 5>{1.0, 1.5, -1.0, 2.0, -4.5};
		auto v1 = array<f64, 5>{-1.0, 1.2, 3.1, 2.0, 4.0};

		auto sum = array<f64, 5>{v0[0] + v1[0], v0[1] + v1[1], v0[2] + v1[2], v0[3] + v1[3], v0[4] + v1[4]};
		auto sub = array<f64, 5>{v0[0] - v1[0], v0[1] - v1[1], v0[2] - v1[2], v0[3] - v1[3], v0[4] - v1[4]};
		auto mul = array<f64, 5>{v0[0] * v1[0], v0[1] * v1[1], v0[2] * v1[2], v0[3] * v1[3], v0[4] * v1[4]};
		auto div = array<f64, 5>{v0[0] / v1[0], v0[1] / v1[1], v0[2] / v1[2], v0[3] / v1[3], v0[4] / v1[4]};

		auto lt = array<bool, 5>{v0[0] < v1[0], v0[1] < v1[1], v0[2] < v1[2], v0[3] < v1[3], v0[4] < v1[4]};
		auto gt = array<bool, 5>{v0[0] > v1[0], v0[1] > v1[1], v0[2] > v1[2], v0[3] > v1[3], v0[4] > v1[4]};
		auto lteq = array<bool, 5>{v0[0] <= v1[0], v0[1] <= v1[1], v0[2] <= v1[2], v0[3] <= v1[3], v0[4] <= v1[4]};
		auto gteq = array<bool, 5>{v0[0] >= v1[0], v0[1] >= v1[1], v0[2] >= v1[2], v0[3] >= v1[3], v0[4] >= v1[4]};
		auto eq = array<bool, 5>{v0[0] == v1[0], v0[1] == v1[1], v0[2] == v1[2], v0[3] == v1[3], v0[4] == v1[4]};
		auto neq = array<bool, 5>{v0[0] != v1[0], v0[1] != v1[1], v0[2] != v1[2], v0[3] != v1[3], v0[4] != v1[4]};

		auto neg0 = array<f64, 5>{-v0[0], -v0[1], -v0[2], -v0[3], -v0[4]};
		auto neg1 = array<f64, 5>{-v1[0], -v1[1], -v1[2], -v1[3], -v1[4]};

		Tp((v0 + v1).equals(sum));
		Tp((v0 - v1).equals(sub));
		Tp((v0 * v1).equals(mul));
		Tp((v0 / v1).equals(div));

		Tp((-v0).equals(neg0));
		Tp((-v1).equals(neg1));

		Tp((v0 == v1).equals(eq));
		Tp((v0 != v1).equals(neq));
		Tp((!(v0 == v1)).equals(v0 != v1));
		Tp((v0 > v1).equals(gt));
		Tp((v0 >= v1).equals(gteq));
		Tp((v0 < v1).equals(lt));
		Tp((v0 <= v1).equals(lteq));
	}

	{
		auto v0 = array<u32, 5>{4, 1, 3, 6, 2};
		auto v1 = array<u32, 5>{1, 1, 8, 9, 0};

		auto and_ = array<u32, 5>{v0[0] & v1[0], v0[1] & v1[1], v0[2] & v1[2], v0[3] & v1[3], v0[4] & v1[4]};
		auto or_  = array<u32, 5>{v0[0] | v1[0], v0[1] | v1[1], v0[2] | v1[2], v0[3] | v1[3], v0[4] | v1[4]};
		auto xor_ = array<u32, 5>{v0[0] ^ v1[0], v0[1] ^ v1[1], v0[2] ^ v1[2], v0[3] ^ v1[3], v0[4] ^ v1[4]};
		auto srl  = array<u32, 5>{v0[0] >> v1[0], v0[1] >> v1[1], v0[2] >> v1[2], v0[3] >> v1[3], v0[4] >> v1[4]};
		auto sll  = array<u32, 5>{v0[0] << v1[0], v0[1] << v1[1], v0[2] << v1[2], v0[3] << v1[3], v0[4] << v1[4]};
		auto not0 = array<u32, 5>{~v0[0],  ~v0[1],  ~v0[2],  ~v0[3],  ~v0[4]};
		auto not1 = array<u32, 5>{~v1[0],  ~v1[1],  ~v1[2],  ~v1[3],  ~v1[4]};


		auto sum = array<u32, 5>{v0[0] + v1[0], v0[1] + v1[1], v0[2] + v1[2], v0[3] + v1[3], v0[4] + v1[4]};
		auto sub = array<u32, 5>{v0[0] - v1[0], v0[1] - v1[1], v0[2] - v1[2], v0[3] - v1[3], v0[4] - v1[4]};
		auto mul = array<u32, 5>{v0[0] * v1[0], v0[1] * v1[1], v0[2] * v1[2], v0[3] * v1[3], v0[4] * v1[4]};


		Tp((v0 & v1).equals(and_));
		Tp((v0 | v1).equals(or_));
		Tp((v0 ^ v1).equals(xor_));
		Tp((v0 >> v1).equals(srl));
		Tp((v0 << v1).equals(sll));
		Tp((~v0).equals(not0));
		Tp((~v1).equals(not1));

		Tp((v0 + v1).equals(sum));
		Tp((v0 - v1).equals(sub));
		Tp((v0 * v1).equals(mul));

		Tp(v0.count([](u32 x){ return (x % 2) == 0; }) == 3)

		Tp(v0.map([](u32 x){ return (x % 2) == 0; }).equals({true, false, false, true, true}))

		Tp(v0.fold([](u32 a, u32 b){ return a + b; }, 0) == 16);
		Tp(v0.fold([](u32 a, u32 b){ return a * b; }, 1) == 144);
	}
	{
		auto v0   = array<i32, 5>{4, 1, 3, 6, 2};
		auto v1   = array<i32, 5>{1, 1, 8, 9, 1};
		auto div  = array<i32, 5>{v0[0] / v1[0], v0[1] / v1[1], v0[2] / v1[2], v0[3] / v1[3], v0[4] / v1[4]};
		auto mod  = array<i32, 5>{v0[0] % v1[0], v0[1] % v1[1], v0[2] % v1[2], v0[3] % v1[3], v0[4] % v1[4]};
		auto neg0 = array<i32, 5>{-v0[0], -v0[1], -v0[2], -v0[3], -v0[4]};
		auto neg1 = array<i32, 5>{-v1[0], -v1[1], -v1[2], -v1[3], -v1[4]};
		Tp((-v0).equals(neg0));
		Tp((-v1).equals(neg1));
		Tp((v0 / v1).equals(div));
		Tp((v0 % v1).equals(mod));
	}
	{
		auto v0 = array<i32, 5>{4, 1, 3, 6, 2};
		i32 a = 3;

		auto and_ = array<i32, 5>{v0[0] & a, v0[1] & a, v0[2] & a, v0[3] & a, v0[4] & a};
		auto or_  = array<i32, 5>{v0[0] | a, v0[1] | a, v0[2] | a, v0[3] | a, v0[4] | a};
		auto xor_ = array<i32, 5>{v0[0] ^ a, v0[1] ^ a, v0[2] ^ a, v0[3] ^ a, v0[4] ^ a};
		auto srl  = array<i32, 5>{v0[0] >> a, v0[1] >> a, v0[2] >> a, v0[3] >> a, v0[4] >> a};
		auto sll  = array<i32, 5>{v0[0] << a, v0[1] << a, v0[2] << a, v0[3] << a, v0[4] << a};

		auto sum = array<i32, 5>{v0[0] + a, v0[1] + a, v0[2] + a, v0[3] + a, v0[4] + a};
		auto sub = array<i32, 5>{v0[0] - a, v0[1] - a, v0[2] - a, v0[3] - a, v0[4] - a};
		auto mul = array<i32, 5>{v0[0] * a, v0[1] * a, v0[2] * a, v0[3] * a, v0[4] * a};
		auto div  = array<i32, 5>{v0[0] / a, v0[1] / a, v0[2] / a, v0[3] / a, v0[4] / a};
		auto mod  = array<i32, 5>{v0[0] % a, v0[1] % a, v0[2] % a, v0[3] % a, v0[4] % a};

		Tp((v0 + a).equals(sum));
		Tp((v0 - a).equals(sub));
		Tp((v0 * a).equals(mul));
		Tp((v0 / a).equals(div));
		Tp((v0 % a).equals(mod));

		Tp((v0 & a).equals(and_));
		Tp((v0 | a).equals(or_));
		Tp((v0 ^ a).equals(xor_));
		Tp((v0 >> a).equals(srl));
		Tp((v0 << a).equals(sll));
	}

	Test_End();
}

	//def gen(symb):
	//  for c in symb:
	//    for i in range(len(a)):
	//      print(f'v0[{i}] {c} v1[{i}]', end = ', ')
	//    print()
	// 
