from interfacer import interface

gen = {
    'internal/allocator.hpp': interface('Allocator', {
        '@include': ['types.hpp', 'slice.hpp', 'mem/mem.hpp'],

        'alloc: void*': ['size: usize', 'align: usize'],
        'dealloc: void': ['ptr: void*'],
        'resize: void*': ['old_ptr: void*', 'old_size: usize', 'new_size: usize', 'align: usize'],
    }),
    'internal/io_reader.hpp': interface('Writer', {
        '@include': ['slice.hpp', 'types.hpp'],
        '@namespace': ['io'],

        'write: usize': ['buffer: slice<byte>'],
    }),
    'internal/io_writer.hpp': interface('Reader', {
        '@include': ['slice.hpp', 'types.hpp'],
        '@namespace': ['io'],

        'read: usize': ['buffer: slice<byte>'],
    }),
}

for file, iface in gen.items():
    iface.namespaces.insert(0, 'x')
    iface.generate_file(outfile=file, guard='ifdef')

